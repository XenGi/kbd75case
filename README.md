# Case for KBD75 v3 PCB

- thin and lightweight
- holes to push out PCB

This work is licensed under a Creative Commons (4.0 International License) Attribution—Noncommercial—Share Alike

---

Made with ❤️ and [solvespace](https://solvespace.com).

