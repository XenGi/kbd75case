$fn = 64;

cutout = 14.2;
1_u = 19.05;
1_25u = 1.25 * 1_u;
1_5u = 1.5 * 1_u;
1_75u = 1.75 * 1_u;
2_25u = 2.25 * 1_u;
6_25u = 6.25 * 1_u;
padding = 1_u / 2;
thickness = 1.3;
margin = -1;
stabilizer_width = 7;
standoff_height = 5;
standoff_diameter = 3;

module stabilizer(x, y, width=2) {
  translate([x, y, 0]) {
    if (width == 2) {
      translate([-12,0,0]) square([stabilizer_width, cutout], center=true);
      translate([12,0,0]) square([stabilizer_width, cutout], center=true);
    } else if (width == 6.25) {
      translate([-50,0,0]) square([stabilizer_width, cutout], center=true);
      translate([50,0,0]) square([stabilizer_width, cutout], center=true);
    }
  }
}


standoffs = [
  [2*1_u, 5.5*1_u],
  [6*1_u, 5.5*1_u],
  [10*1_u, 5.5*1_u],
  [14*1_u, 5.5*1_u],
  [1_5u+1_u, 3.5*1_u],
  [1_5u+7*1_u, 3.5*1_u],
  [1_5u+12*1_u, 3.5*1_u],
  [1_75u+5*1_u, 2.5*1_u],
  [2_25u+1_u, 1.5*1_u],
  [1_25u, 0.5*1_u],
  [3*1_25u+1_u, 0.5*1_u],
  [3*1_25u+6_25u-1_u, 0.5*1_u],
  [14*1_u, 0.5*1_u],
];

for (standoff = standoffs) {
  translate([standoff[0] + margin, standoff[1] + margin, -1*standoff_height+thickness]) cylinder(h=standoff_height, d=standoff_diameter);
}

linear_extrude(thickness) difference() {
  square([2*margin + 16*1_u, 2*margin + 6*1_u]);
  
  // Row 1
  for (i = [0:15]) translate([margin + i*1_u + padding, margin + 5*1_u + padding, 0]) square(cutout, center=true);
  
  // Row 2
  for (i = [0:12]) translate([margin + i*1_u + padding, margin + 4*1_u + padding, 0]) square(cutout, center=true);
  translate([margin + 13*1_u + 2*padding, margin + 4*1_u + padding, 0]) square(cutout, center=true); // Backspace
  stabilizer(margin + 13*1_u + 2*padding, margin + 4*1_u + padding);
  translate([margin + 15*1_u + padding, margin + 4*1_u + padding, 0]) square(cutout, center=true); // Home
  
  // Row 3
  translate([margin + 1_5u/2, margin + 3*1_u + padding, 0]) square(cutout, center=true); // Tab
  for (i = [0:11]) translate([margin + i*1_u + 1_5u + padding, margin + 3*1_u + padding, 0]) square(cutout, center=true);
  translate([margin + 12*1_u + 1_5u + 1_5u/2, margin + 3*1_u + padding, 0]) square(cutout, center=true); // Backslash
  translate([margin + 15*1_u + padding, margin + 3*1_u + padding, 0]) square(cutout, center=true); // Page Up
  
  // Row 4
  translate([margin + 1_75u/2, margin + 2*1_u + padding, 0]) square(cutout, center=true); // Capslock
  for (i = [0:10]) translate([margin + i*1_u + 1_75u + padding, margin + 2*1_u + padding, 0]) square(cutout, center=true);
  translate([margin + 11*1_u + 1_75u + 2_25u/2, margin + 2*1_u + padding, 0]) square(cutout, center=true); // Return
  stabilizer(margin + 11*1_u + 1_75u + 2_25u/2, margin + 2*1_u + padding);
  translate([margin + 15*1_u + padding, margin + 2*1_u + padding, 0]) square(cutout, center=true); // Page Down
  
  // Row 5
  translate([margin + 2_25u/2, margin + 1_u + padding, 0]) square(cutout, center=true); // Left Shift
  stabilizer(margin + 2_25u/2, margin + 1_u + padding);
  for (i = [0:9]) translate([margin + i*1_u + 2_25u + padding, margin + 1_u + padding, 0]) square(cutout, center=true);
  translate([margin + 10*1_u + 2_25u + 1_75u/2, margin + 1_u + padding, 0]) square(cutout, center=true); // Right Shift
  translate([margin + 14*1_u + padding, margin + 1_u + padding, 0]) square(cutout, center=true); // Up
  translate([margin + 15*1_u + padding, margin + 1_u + padding, 0]) square(cutout, center=true); // End
  
  // Row 6
  for (i = [0:2]) translate([margin + i*1_25u + 1_25u/2, margin + padding, 0]) square(cutout, center=true);
  translate([margin + 3*1_25u + 6_25u/2, margin + padding, 0]) square(cutout, center=true); // Spacebar
  stabilizer(margin + 3*1_25u + 6_25u/2, margin + padding, width=6.25);
  for (i = [0:5]) translate([margin + i*1_u + 3*1_25u + 6_25u + padding, margin + padding, 0]) square(cutout, center=true);
}
